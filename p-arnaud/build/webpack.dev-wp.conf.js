var utils = require('./utils')
var webpack = require('webpack')
var config = require('../config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var htmlInjector = require('bs-html-injector');
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
var StylelintPlugin = require('stylelint-webpack-plugin')
var webpackDevMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
var BrowserSync = require('browser-sync')
var WriteFilePlugin = require('write-file-webpack-plugin')

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

console.log(utils.assetsPath('css/styles.css'))

// Webpack config to use for Wp configuration.
var webpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({
      sourceMap: config.dev.cssSourceMap,
      extract: true
    })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: '#cheap-module-eval-source-map',
  output: {
    path: config.build.assetsRoot,
    filename: utils.assetsPath('js/[name].js'),
    chunkFilename: utils.assetsPath('js/[id].js'),
    publicPath: '/assets/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    new ExtractTextPlugin({
      filename: utils.assetsPath('css/styles.css')
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.NoEmitOnErrorsPlugin(),
    new StylelintPlugin({
      files: ['**/*.vue', '**/*.scss']
    }),
    new FriendlyErrorsPlugin(),
    new WriteFilePlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
})

var bundler = webpack(webpackConfig)
BrowserSync.use(htmlInjector, { restrictions: [ '#app' ] })

// Create the browserSync.
BrowserSync.init({
  files: [{
    // scss|js managed by webpack
    // optionally exclude other managed assets: images, fonts, etc
    match: [ 'src/styles/**/*\.scss' ],

    // manually sync everything else
    fn: synchronize,
  }],
  proxy: {
    // proxy local WP install
    target: config.devWp.wpUrl,

    middleware: [
      // converts browsersync into a webpack-dev-server
      webpackDevMiddleware(bundler, {
        publicPath: webpackConfig.output.publicPath,
        noInfo: true,
        stats: {
      		colors: true
      	}
      }),

      // hot update js &amp;&amp; css
      webpackHotMiddleware(bundler),
    ],
  },
});

var synchronize = (event, file) => {
  htmlInjector();
}