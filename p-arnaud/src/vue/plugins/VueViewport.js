import _ from 'lodash'

const VueViewport = {}

VueViewport.install = (Vue, options) => {
  const store = options.store

  const size = {
    width: window.innerWidth,
    height: window.innerHeight
  }

  store.dispatch('checkViewport', size)

  window.addEventListener('resize', _.throttle(() => {
    const resize = {
      width: window.innerWidth,
      height: window.innerHeight
    }

    store.dispatch('checkViewport', resize)
    store.commit('setViewport', resize)
  }, 100))
}

module.exports = VueViewport
