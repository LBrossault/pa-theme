/**
 * @Plugin
 *
 * VueData
 * Defined many methods to use requests and manage Vuex Store
 */

import Vue from 'vue'
import VueResource from 'vue-resource'
import _ from 'lodash'

Vue.use(VueResource)
Vue.http.options.root = wp.root

const urls = {
	menu: 'wp-api-menus/v2/menus/2',
	socials: 'wp-api-menus/v2/menus/9',
	categories: 'wp/v2/categories',
	page: 'wp/v2/pages',
	post: 'wp/v2/posts',
	postAll: 'wp/v2/posts/?per_page=100',
	contact: 'wp/v2/pages?slug=contact'
}

export default {
	install (Vue, options) {
		const store = options.store

		/*
		*	Get Data
		*/
	 	Vue.prototype.get = (component, storeFunc, id, category) => {
	    let url = urls[component]

	    if (id) {
	    	url = `${urls[component]}/${id}`
	    }

	    if (category) {
	    	const cat = _.find(store.getters.getCategoriesData, { label: category })
	    	url = `${urls[component]}?categories=${cat.id}&&per_page=100`
	    }

	    return Vue.http.get(url)
	      .then((response) => {
	      	if (storeFunc) {
	      		store.commit(storeFunc, response)
	      	}

	        return Promise.resolve(response)
	      })
	      .catch((error) => {
	        return Promise.reject(error)
	      })
	  }
	}
}

