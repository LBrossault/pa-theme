/**
 * @Plugin
 *
 * VueRoutes
 * Store id routes
 */

export default {
	install (Vue, options) {
		const store = options.store

		store.commit('setRoutesId', options.routesId)
	}
}

