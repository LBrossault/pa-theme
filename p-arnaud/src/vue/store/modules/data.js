/**
 * Initial state
 */
const state = {
	menuData: null,
	categoriesData: null,
	categoriesContent: {},
	routesData: [],
	homeData: null,
	contactData: null,
	formData: {},
	menuState: false,
	footerData: false,
	postCategory: false
}

/**
 * Getters
 */
const getters = {
	getMenuData: state => state.menuData,
	getCategoriesData: state => state.categoriesData,
	getHomeData: state => state.homeData,
	getRoutesData: state => state.routesData,
	getRouteWithID: (state) => (key) => state.routesData[key],
	getCategorieContent: (state) => (key) => state.categoriesContent[key],
	getAllCategoriesContent: (state) => state.categoriesContent,
	getFormData: (state) => state.formData,
	getMenuState: (state) => state.menuState,
	getFooterData: (state) => state.footerData,
	getPostCategory: (state) => state.postCategory,
	getContactData: (state) => state.contactData
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
	setMenuData (state, payload) {
		state.menuData = payload
	},
	setRoutesData (state, payload) {
		const processedData = state.routesData
		processedData[payload.key] = payload.value

		state.routesData = Object.assign({}, processedData)
	},
	setHomeData (state, payload) {
		state.homeData = payload
	},
	setCategoriesData (state, payload) {
		const categories = []

		for (let y = 0; y < payload.body.length; y++) {
			const obj = {
				label: payload.body[y].slug,
				id: payload.body[y].id
			}

			categories.push(obj)
		}

		state.categoriesData = categories
	},
	setCategoriesContent (state, payload) {
		state.categoriesContent[payload.key] = payload.value
	},
	setFormData (state, payload) {
		const processedData = state.formData
		processedData[payload.key] = payload.value

		state.formData = Object.assign({}, processedData)
	},
	setMenuState (state, payload) {
		state.menuState = payload
	},
	setFooterData (state, payload) {
		state.footerData = payload
	},
	setPostCategory (state, payload) {
		state.postCategory = payload
	},
	setContactData (state, payload) {
		state.contactData = payload
	}
}

module.exports = {
  state,
  getters,
  actions,
  mutations
}
