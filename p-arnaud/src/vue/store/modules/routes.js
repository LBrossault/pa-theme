/**
 * Initial state
 */
const state = {
	routesId: null,
	routeLeave: false,
	pageTitle: `${wp.site_name} - ${wp.site_name}`
}

/**
 * Getters
 */
const getters = {
	getRoutesId: state => state.routesId,
	getRouteLeave: state => state.routeLeave,
	getPageTitle: state => state.pageTitle
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
	setRoutesId (state, payload) {
		state.routesId = payload
	},
	setRouteLeave (state, payload) {
		state.routeLeave = payload
	},
	setPageTitle (state, payload) {
		state.pageTitle = payload
	}
}

module.exports = {
  state,
  getters,
  actions,
  mutations
}
