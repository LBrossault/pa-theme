const state = {
  vwWidth: window.innerWidth,
  vwHeight: window.innerHeight,
  browser: null,
  smallDesktop: false,
  tablet: false,
  mobile: false,
  breakpoints: {
    desktop: 1200,
    tablet: 960,
    mobile: 680
  }
}

/**
 * Getters
 */
const getters = {
  getViewport: (state) => {
    return {
      width: state.vwWidth,
      height: state.vwHeight
    }
  },
  getBreakpoints: state => state.breakpoints,
  getSmallDesktop: state => state.smallDesktop,
  getTablet: state => state.tablet,
  getMobile: state => state.mobile
}

/**
 * Actions
 */
const actions = {
  checkViewport ({ commit, state }) {
    if (window.innerWidth <= state.breakpoints.desktop) {
      commit('setSmallDesktop', true)
    } else {
      commit('setSmallDesktop', false)
    }

    if (window.innerWidth <= state.breakpoints.tablet) {
      commit('setTablet', true)
    } else {
      commit('setTablet', false)
    }

    if (window.innerWidth <= state.breakpoints.mobile) {
      commit('setMobile', true)
    } else {
      commit('setMobile', false)
    }
  }
}

/**
 * Mutations
 */
const mutations = {
  setViewport (state, payload) {
    state.vwWidth = payload.width
    state.vwHeight = payload.height
  },
  setSmallDesktop (state, payload) {
    state.smallDesktop = payload
  },
  setTablet (state, payload) {
    state.tablet = payload
  },
  setMobile (state, payload) {
    state.mobile = payload
  }
}

module.exports = {
  state,
  getters,
  actions,
  mutations
}
