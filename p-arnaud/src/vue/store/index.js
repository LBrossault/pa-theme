import Vue from 'vue'
import Vuex from 'vuex'

import data from './modules/data'
import device from './modules/device'
import routes from './modules/routes'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
		data,
		device,
		routes
  }
})
