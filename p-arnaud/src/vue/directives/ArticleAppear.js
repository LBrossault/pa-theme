import ScrollReveal from 'scrollreveal'
import Vue from 'vue'

let sr = new ScrollReveal({
  scale: false,
  enter: 'bottom',
  move: '8px',
  reset: false,
  viewFactor: 0.2
})

Vue.directive('article-appear', {
  bind: (elt, binding, vnode) => {
    sr.reveal(elt)
  }
})