/*
* Get content type ID
*/

import _ from 'lodash'
import store from '~store'
import PageTitleMixin from './PageTitle'

let _route = null

export default {
	mixins: [PageTitleMixin],
	created () {
		if (_route) {
			this.fetchData()
		}
	},
	beforeRouteEnter (to, from, next) {
		const slug = to.name
		const routesID = store.getters.getRoutesId

		const route = _.find(routesID, { slug })

		if (route) {
			_route = route

			store.commit('setRouteLeave', false)
			next()
		}
	},
	beforeRouteLeave (to, from, next) {
		store.commit('setRouteLeave', true)

		setTimeout(() => {
			next()
		}, 500)
	},
	data () {
		return {
			contentData: null,
			articlesData: {},
			routeName: this.$route.name,
			dataReady: false
		}
	},
	methods: {
		fetchData () {
			this.dataReady = 0
			this.contentData = null
			this.articlesData = {}

			this.routeName = this.$route.name
			this.dispatchPageTitle(this.routeName)

			if (!this.$store.getters.getRouteWithID(`r-${_route['id']}`)) {
				this.get(_route['type'], null, _route['id']).then((res) => {
					this.$store.commit('setRoutesData', {
						key: `r-${_route['id']}`,
						value: res.body
					})

					this.contentData = res.body

					if (_route['type'] === 'post') {
						this.setMenu()
					} else {
						this.$store.commit('setPostCategory', null)
					}

					if (this.contentData.acf.category_page !== 'none') {
						this.get('post', null, null, this.contentData.acf.category_page).then((res) => {
							this.articlesData = res

							this.$store.commit('setCategoriesContent', {
								key: this.contentData.acf.category_page,
								value: res
							})

							this.dataReady = 1
						})
					} else {
						this.dataReady = 1
					}
				})
			} else {
				const self = this

				setTimeout(() => {
					self.contentData = self.$store.getters.getRouteWithID(`r-${_route['id']}`)

					if (_route['type'] === 'post') {
						self.setMenu()
					} else {
						self.$store.commit('setPostCategory', null)
					}

					if (self.contentData.acf.category_page !== 'none') {
						/*
						* Fix reactivity
						*/
						setTimeout(() => {
							self.articlesData = Object.assign({}, self.articlesData, self.$store.getters.getCategorieContent(self.contentData.acf.category_page))
							self.dataReady = 1
						}, 0)
					} else {
						self.dataReady = 1
					}
				}, 800)
			}
		},
		updateTitle () {}
	},
	watch: {
    '$route': 'fetchData'
  }
}
