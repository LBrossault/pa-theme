export default {
	methods: {
		dispatchPageTitle (title) {
			if (title) {
				const pageName = this.capitalize(title)
				const fullTitle = `${pageName} - ${wp.site_name}`

				this.$store.commit('setPageTitle', fullTitle)
			}
		},
		capitalize (word) {
    	return word.charAt(0).toUpperCase() + word.slice(1)
		}
	}
}
