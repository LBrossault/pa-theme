import _ from 'lodash'
import selectParent from 'select-parent'

export default {
	data () {
		this.checkPath('init')

		return {
			isHome: this.initIsHome,
			niv1Target: null,
			niv1Path: this.initNiv1Path
		}
	},
	methods: {
		setHover (e) {
			const parent = selectParent('.li-niv-1', e.target)
			let el

			if (parent) {
				el = parent
			} else {
				el = e.target
			}

			this.niv1Target = parseInt(el.getAttribute('data-index'))
		},
		resetHover () {
			this.niv1Target = null
		},
		/*
		* Check if menu is active
		*/
		checkPath (init) {
			_.find(this.$store.getters.getMenuData.body.items, (item, index) => {
				if (this.$route.path.indexOf(item.object_slug) > -1) {
					if (init === 'init') {
						this.initNiv1Path = index + 1
					} else {
						this.niv1Path = index + 1
					}

					return true
				}

				return null
			})

			if (init === 'init') {
				this.initIsHome = this.$route.path === '/'
			} else {
				this.isHome = this.$route.path === '/'

				if (this.$route.path === '/') {
					this.niv1Path = null
				}
			}
		}
	},
	watch: {
    '$route': 'checkPath'
  },
	computed: {
		getMenuData () {
			return this.$store.getters.getMenuData
		},
		getPostCategory () {
			return this.$store.getters.getPostCategory
		}
	}
}
