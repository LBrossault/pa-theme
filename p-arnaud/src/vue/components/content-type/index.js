import Vue from 'vue'

/*
* Content type
*/

import Home from './Home'
import Contact from './Contact'
import PageType from './Page'
import PostType from './Post'

let contentType = {
	Home: Vue.component('home', Home),
	Contact: Vue.component('contact', Contact),
  PageType: Vue.component('page-type', PageType),
  PostType: Vue.component('post-type', PostType)
}

export { contentType }
