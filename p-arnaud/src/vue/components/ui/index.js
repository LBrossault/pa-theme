import Vue from 'vue'

/*
* Common
*/

import InputComp from './InputComp'
import ImageComp from './ImageComp'

let ui = {
	InputComp: Vue.component('input-comp', InputComp),
	ImageComp: Vue.component('image-comp', ImageComp)
}

export { ui }
