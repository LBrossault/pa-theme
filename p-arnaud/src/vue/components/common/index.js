import Vue from 'vue'

/*
* Common
*/

import ArticleView from './article/ArticleView'
import HeaderComp from './header/Header'
import FooterComp from './footer/Footer'
import CapsuleArticle from './article/Capsule'
import Loader from './content/Loader'
import MenuMobile from './header/MenuMobile'

let common = {
	ArticleView: Vue.component('article-view', ArticleView),
  HeaderComp: Vue.component('header-comp', HeaderComp),
  FooterComp: Vue.component('footer-comp', FooterComp),
  CapsuleArticle: Vue.component('capsule-article', CapsuleArticle),
  Loader: Vue.component('loader', Loader),
  MenuMobile: Vue.component('menu-mobile', MenuMobile)
}

export { common }
