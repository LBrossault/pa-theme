import { common } from './common'
import { contentType } from './content-type'

export let components = {
	contentType,
	common
}
