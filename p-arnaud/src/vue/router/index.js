import Vue from 'vue'
import Router from 'vue-router'

import { contentType } from '~components/content-type'

Vue.use(Router)

let router = {
	routes: null,
	ids: []
}

const _routes = []

for (let key in wp.routes) {
	const route = wp.routes[key]
	let component

	if (route.type === 'post') {
		component = contentType.PostType
	} else {
		component = contentType.PageType
	}

	if (wp.base_url.indexOf(route.slug) > -1) {
		component = contentType.Home
	}

	if (route.slug === 'contact') {
		component = contentType.Contact
	}

	const objRoute = {
		path: `${wp.base_path}${route.slug}`,
		name: route.slug,
		component: component,
    meta: {
		  title: route.slug.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' '),
      analytics: {
        pageviewTemplate (route) {
          return {
            title: route.slug.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' '),
            path: route.path,
            location: wp.base_path
          }
        }
      }
    }
	}

	router.ids.push({
		slug: route.slug,
		id: route.id,
		type: route.type
	})

	_routes.push(objRoute)
}

_routes.push({
	path: '/',
	name: 'home',
	component: contentType.Home,
  meta: {
    title: 'Home',
    analytics: {
      pageviewTemplate (route) {
        return {
          title: 'Home',
          path: route.path,
          location: wp.base_path
        }
      }
    }
  }
})

router.routes = new Router({
	mode: 'history',
  routes: _routes
})

export default router
