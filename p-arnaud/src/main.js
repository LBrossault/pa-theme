// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import App from './vue/App'
import router from './vue/router'
import store from './vue/store'

import VueData from '~plugins/VueData'
import VueRoutes from '~plugins/VueRoutes'
import VueViewport from '~plugins/VueViewport'

import { components } from '~components'

Vue.config.productionTip = false

Vue.use(VueData, { store })
Vue.use(VueViewport, { store })
Vue.use(VueRoutes, { store, routesId: router.ids })

Vue.use(VueAnalytics, {
  id: 'UA-109756248-1',
  router: router.routes,
  checkDuplicatedScript: true
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router.routes,
  store,
  template: '<App/>',
  components: { App, components }
})
