<?php

add_theme_support( 'title-tag' );

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function rest_theme_scripts() {
	$base_url  = esc_url_raw( home_url() );
	$base_path = rtrim( parse_url( $base_url, PHP_URL_PATH ), '/' );

	// wp_enqueue_script( 'rest-theme-vue-manifest', get_template_directory_uri() . '/assets/build/js/manifest.js', array(), '1.0.0', true );
	// wp_enqueue_script( 'rest-theme-vue-vendor', get_template_directory_uri() . '/assets/build/js/vendor.js', array(), '1.0.0', true );
	wp_enqueue_script( 'rest-theme-vue-app', get_template_directory_uri() . '/assets/dev/js/app.js', array(), '1.0.0', true );

	wp_localize_script( 'rest-theme-vue-app', 'wp', array(
		'root'      => esc_url_raw( rest_url() ),
		'base_url'  => $base_url,
		'base_path' => $base_path ? $base_path . '/' : '/',
		'nonce'     => wp_create_nonce( 'wp_rest' ),
		'site_name' => get_bloginfo( 'name' ),
		'routes'    => rest_theme_routes(),
	) );
}

add_action( 'wp_enqueue_scripts', 'rest_theme_scripts' );


function rest_theme_styles() {
  wp_enqueue_style( 'rest-theme-vue-manifest', get_template_directory_uri() . '/assets/build/css/app.css' );
}  

add_action( 'wp_enqueue_scripts', 'rest_theme_styles' );

function rest_theme_routes() {
	$routes = array();

	$query = new WP_Query( array(
		'post_type'      => 'any',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	) );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$routes[] = array(
				'id'   => get_the_ID(),
				'type' => get_post_type(),
				'slug' => basename( get_permalink() ),
			);
		}
	}
	wp_reset_postdata();

	return $routes;
}

function my_mce_before_init( $init_array ) {
	$init_array['theme_advanced_styles'] =
            '.translation=translation;.contributors=contributors;.notes=notes;';

	return $init_array;
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );
