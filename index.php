<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Pascale Arnaud - Photographie">
    <meta name="description" content="Pascale Arnaud Photographe - Portfolio de Pascale Arnaud. Prix picto 2017. Portraits, Fine-arts, fashion.">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/assets/img/favicon.ico" type="image/x-icon">
    <?php wp_head(); ?>
    <meta property="og:type" content="website">
    <meta property="og:title" content="Pascale Arnaud">
    <meta property="og:url" content="https://pascalearnaud.fr/">
    <meta property="og:site_name" content="Pascale Arnaud">
    <?php
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                $articleData = get_field('main_picture_project', get_the_ID());
                if ($articleData['url']) {
    ?>
    <meta property="og:image" content="<?php echo($articleData['url']); ?>">
    <meta property="og:image:secure_url" content="<?php echo($articleData['url']); ?>">
    <meta name="twitter:image" content="<?php echo($articleData['url']); ?>">
    <?php
                } else {
    ?>
        <meta property="og:image" content="https://pascalearnaud.fr/wp-content/uploads/CT_Pascale_Arnaud_2.jpg">
        <meta property="og:image:secure_url" content="https://pascalearnaud.fr/wp-content/uploads/CT_Pascale_Arnaud_2.jpg">
        <meta name="twitter:image" content="https://pascalearnaud.fr/wp-content/uploads/CT_Pascale_Arnaud_2.jpg">
    <?php
                }
            endwhile;
        endif;
    ?>
    <meta property="og:image:width" content="5440">
    <meta property="og:image:height" content="3840">
    <meta property="og:locale" content="fr_FR">
    <meta name="twitter:site" content="https://pascalearnaud.fr/">
    <meta name="twitter:text:title" content="Pascale Arnaud">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:description" content="Photographe - Portfolio de Pascale Arnaud. Portraits, Fine-arts, fashion.">
</head>
<body>
    <div id="content">
        <?php
        if ( have_posts() ) :

            if ( is_home() && ! is_front_page() ) {
                echo '<h1>' . single_post_title( '', false ) . '</h1>';
            }

            while ( have_posts() ) : the_post();

                if ( is_singular() ) {
                    the_title( '<h1>', '</h1>' );
                } else {
                    the_title( '<h2><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
                }

                the_content();

            endwhile;

        endif;
        ?>
    </div>

    <div id="app"></div>

    <?php wp_footer(); ?>

</body>
</html>